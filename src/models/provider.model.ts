import mongoose from 'mongoose';

export enum providerStatus {
  active = 'active',
  inactive = 'inactive'
}

export interface ProviderDocument extends mongoose.Document {
  name: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}

const providerSchema = new mongoose.Schema(
  {
    name: { type: String },
    status: {
      type: String,
      enum: Object.values(providerStatus),
      default: providerStatus.inactive,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const ProviderModel = mongoose.model('Provider', providerSchema);

export default ProviderModel;
