import mongoose from 'mongoose';
import { ProviderDocument } from './provider.model'
import shortid from 'shortid';

export enum types {
  transactional = 'transactional',
  nonTransactional = 'nonTransactional',
}

export enum transactionStatus {
  active = 'success',
  initial = 'initial',
  failed = 'failed',
}

export interface TransactionInput {
  provider: ProviderDocument["_id"];
  to: Array<string>;
  from: string;
  message: string;
  transRef: string;
  uniqueRef: string;
  type: string;
  status: string;
  walletId: string
}

export interface TransactionDocument extends TransactionInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const transactionSchema = new mongoose.Schema(
  {
    provider: { type: mongoose.Schema.Types.ObjectId, ref: 'Provider' },
    to: { type: Array },
    from: { type: String },
    message: { type: String },
    transRef: { type: String },
    walletId: { type: String },
    type: { type: String, enum: Object.values(types),  required: true },
    uniqueRef: { type: String, default: shortid.generate },
    status: {
      type: String,
      enum: Object.values(transactionStatus),
      default: transactionStatus.initial,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const TransactionModel = mongoose.model('Transaction', transactionSchema);

export default TransactionModel;
