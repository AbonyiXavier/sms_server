import { Request, Response } from 'express';
import { createTransaction, fetchTransactions } from '../services/transaction.service';
import {
  createdResponse,
  badRequestResponse,
  serverErrorResponse,
  successfulResponse,
} from '../helpers/response';
export default class TransactionController {
  static async createTransactionHandler(req: Request, res: Response) {
    try {
      const data = await createTransaction(req.body);

      if (!data?.status) {
        return badRequestResponse({
          res,
          message: data?.message,
        });
      }

      return createdResponse({
        res,
        message: data?.message,
        data,
      });
    } catch (error) {
      console.log('err', error);
      return serverErrorResponse({
        res,
        message: 'something went wrong',
      });
    }
  }

  static async fetchTransactionHandler(req: Request, res: Response) {
    try {
      let { search, page, limit } = req.query;

      const { status, message, data, meta } = await fetchTransactions(search, page, limit);

      if (!status) {
        return badRequestResponse({
          res,
          message,
        });
      }

      return successfulResponse({
        res,
        message,
        meta,
        data,
      });
    } catch (error) {
      console.log('err', error);
      return serverErrorResponse({
        res,
        message: 'something went wrong',
      });
    }
  }
}
