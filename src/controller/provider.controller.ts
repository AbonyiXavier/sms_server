import { Request, Response } from 'express';
import { createProvider, fetchProviders, swtichProviderStatus } from '../services/provider.service';
import { createdResponse, badRequestResponse, successfulResponse, serverErrorResponse } from '../helpers/response';

export default class ProviderController {
  static async createProviderHandler(req: Request, res: Response) {
    try {
      const { status, message, data } = await createProvider(req.body);
      if (!status) {
        return badRequestResponse({
          res,
          message,
        });
      }

      return createdResponse({
        res,
        message,
        data,
      });
    } catch (error) {
      console.log('err', error);
      return serverErrorResponse({
        res,
        message: 'something went wrong',
      });
    }
  }

  static async fetchProviderHandler(req: Request, res: Response) {
    try {
      let { search, page, limit } = req.query;

      const { status, message, data, meta } = await fetchProviders(search, page, limit);

      if (!status) {
        return badRequestResponse({
          res,
          message,
        });
      }

      return successfulResponse({
        res,
        message,
        meta,
        data,
      });
    } catch (error) {
      console.log('err', error);
      return serverErrorResponse({
        res,
        message: 'something went wrong',
      });
    }
  }

  static async updateProviderStatusHandler(req: Request, res: Response) {
    try {
        let { id } = req.params;

      const { status, message, data } = await swtichProviderStatus(id);

      if (!status) {
        return badRequestResponse({
          res,
          message,
        });
      }

      return successfulResponse({
        res,
        message,
        data
      });
    } catch (error) {
      console.log('err', error);
      return serverErrorResponse({
        res,
        message: 'something went wrong',
      });
    }
  }
}
