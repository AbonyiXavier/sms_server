import { Express, Request, Response } from 'express';
import ProviderController from './controller/provider.controller';
import TransactionController from './controller/transaction.controller';

const { createProviderHandler, fetchProviderHandler, updateProviderStatusHandler } = ProviderController;
const { createTransactionHandler, fetchTransactionHandler } = TransactionController;

function routes(app: Express) {
  app.get('/check', (req: Request, res: Response) => res.sendStatus(200));

  // Providers routes
  app.post('/api/providers', createProviderHandler);

  app.get('/api/providers', fetchProviderHandler);

  app.patch('/api/providers/:id', updateProviderStatusHandler);

  // Transactions Routes
  app.post('/api/transactions', createTransactionHandler);

  app.get('/api/transactions', fetchTransactionHandler);
}

export default routes;
