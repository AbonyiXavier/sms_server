import mongoose from "mongoose";
import logger from "./logger"

require('dotenv').config();


async function connect() {
    const dbUri = <string>process.env.MONGO_URI;

    try {
        await mongoose.connect(dbUri, { useNewUrlParser: true, useUnifiedTopology: true });
        logger.info("Db connected")
    } catch (error) {
        logger.error("Could not connect to db")
        process.exit(1)
    }
}

export default connect;