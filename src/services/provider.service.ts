import ProviderModel from '../models/provider.model';
import logger from '../utils/logger';

export async function createProvider(name: string) {
  try {
    const provider = await ProviderModel.create(name);
    return {
      status: true,
      message: 'Provider added successfully',
      data: provider,
    };
  } catch (error: any) {
    console.log('error out', error);
    logger.error(error);
    return {
      status: false,
      message: 'Something went wrong',
    };
  }
}

export async function fetchProviders(search: any, page: any, limit: any) {
  try {
    page = page < 1 ? 1 : page;
    limit = 10;

    const searchQueries = {
      $and: [
        {
          $or: [
            {
              name: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              status: { $regex: new RegExp(search), $options: 'i' },
            },
          ],
        },
      ],
    };

    // get total documents in the providers collection
    let count = await ProviderModel.countDocuments();
    let totalPages = Math.ceil(count / limit);
    page = page > totalPages ? totalPages : page;

    const transaction = await ProviderModel.find(searchQueries, { __v: 0 })
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec();
    return {
      status: true,
      message: 'Providers fetched successfully',
      meta: {
        totalPages: totalPages,
        currentPage: page,
        totalProviders: count,
      },
      data: transaction,
    };
  } catch (error) {
    console.log('error out', error);
    logger.error(error);
    return {
      status: false,
      message: 'Something went wrong',
    };
  }
}

export async function swtichProviderStatus(id: string) {
  try {
    const provider = await ProviderModel.findOne({ _id: id }).exec();

    if (provider.status === 'inactive') {
      const updatedProvider = await ProviderModel.findOneAndUpdate(
        {
          _id: id,
        },
        {
          $set: { status: 'active' },
        },
        {
          new: true,
          useFindAndModify: false,
        }
      );

      return {
        status: true,
        message: 'Status updated successfully',
        data: updatedProvider,
      };
    }

    const updated = await ProviderModel.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: { status: 'inactive' },
      },
      {
        new: true,
        useFindAndModify: false,
      }
    );

    return {
      status: true,
      message: 'Status updated successfully',
      data: updated,
    };
  } catch (error) {
    console.log('error out', error);
    logger.error(error);
    return {
      status: false,
      message: 'Something went wrong',
    };
  }
}
