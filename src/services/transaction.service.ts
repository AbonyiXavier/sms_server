import TransactionModel, {
  TransactionDocument,
  TransactionInput,
} from '../models/transaction.model';
import logger from '../utils/logger';

export async function createTransaction(input: TransactionInput) {
  try {
    if (input.type === 'nonTransactional' && !input.walletId) {
      return {
        status: false,
        message: 'walletId is required',
      };
    }
    if (input.type === 'nonTransactional' || input.type === 'transactional') {
      const transaction = await TransactionModel.create(input);

      return {
        status: true,
        message: 'Created successfully',
        data: transaction,
      };
    }
  } catch (error: any) {
    console.log('error out', error);
    logger.error(error);
    return {
      status: false,
      message: 'Something went wrong',
    };
  }
}

export async function fetchTransactions(search: any, page: any, limit: any) {
  try {
    page = page < 1 ? 1 : page;    
    limit = 10;

    const searchQueries = {
      $and: [
        {
          $or: [
            {
              to: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              from: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              message: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              transRef: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              uniqueRef: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              walletId: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              type: { $regex: new RegExp(search), $options: 'i' },
            },
            {
              status: { $regex: new RegExp(search), $options: 'i' },
            },
          ],
        },
      ],
    };

    // get total documents in the Transactions collection
    let count = await TransactionModel.countDocuments();
    let totalPages = Math.ceil(count / limit);
    page = page > totalPages ? totalPages : page;
    
    const transaction = await TransactionModel.find(searchQueries, { __v: 0 })
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .populate('provider')
      .exec();
    return {
      status: true,
      message: 'Transactions fetched successfully',
      meta: {
        totalPages: totalPages,
        currentPage: page,
        totalTransactions: count,
      },
      data: transaction,
    };
  } catch (error) {
    console.log('error out', error);
    logger.error(error);
    return {
      status: false,
      message: 'Something went wrong',
    };
  }
}
