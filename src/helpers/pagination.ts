
export const getPagination = (page: any, size: any) => {
    page = parseInt(page, 10);
    size = parseInt(size, 10);
    const limit = size; // By default, 3 Items will be fetched from database in page index 0.
    const offset = (page - 1) * size;
  
    return { limit, offset };
  };
  

  export const getPagingData = ({ count, page, limit }) => {
    const lastPage = Math.ceil(count / limit);
    console.log(lastPage);
    
  
    return { total: count, perPage: limit, page: parseInt(page, 10), lastPage };
  };