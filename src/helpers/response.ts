import { Request, Response} from "express"

interface RDMM {
  res: Response
  message: string
  data?: any
  meta?: any
}

interface RDM {
  res: Response
  message: string
  data?: any
}

interface RDME {
  res: Response
  message: string | undefined
  data?: any
  error?: any
}

interface RMF {
  res: Response
  message: string
  field: string
}

export const successfulResponse = ({ res, data, message, meta }: RDMM) => {
  return res.status(200).json({
    status: true,
    message,
    meta,
    data,
  })
}

export const createdResponse = ({ res, data, message }: RDM) => {
  return res.status(201).json({
    status: true,
    message,
    data,
  })
}

export const deletedResponse = ({ res, data, message }: RDM) => {
  return res.status(204).json({
    status: true,
    message,
    data,
  })
}

export const conflictResponse = ({ res, data, message }: RDM) => {
  return res.status(409).json({
    status: false,
    message,
    data,
  })
}

export const badRequestResponse = ({ res, data, message }: RDME) => {
  return res.status(400).json({
    status: false,
    message,
    data,
  })
}

export const unauthorizedResponse = ({ res, data, message }: RDM) => {
  return res.status(401).json({
    status: false,
    message,
    data,
  })
}

export const forbiddenResponse = ({ res, data, message }: RDM) => {
  return res.status(403).json({
    status: false,
    message,
    data,
  })
}

export const notFoundResponse = ({ res, data, message }: RDM) => {
  return res.status(404).json({
    status: false,
    message,
    data,
  })
}

export const serverErrorResponse = ({ res, data, message }: RDM) => {
  return res.status(500).json({
    status: false,
    message,
    data,
  })
}

export const validationErrorResponse = ({ res, message, field }: RMF) => {
  return res.status(500).json({
    status: false,
    message: 'validation error',
    data: [
      {
        message,
        field,
        validation: 'valid',
      },
    ],
  })
}
